#install.packages("RColorBrewer")
setwd("E:/R_workbook/")
library(RColorBrewer)
#display.brewer.all()
df <- read.table("dataset_volcano.txt",row.names = 1,header = T)
#View(df)
fd <- 0.35 #设置foldchange阈值
cut.fd <- 0.25
pvalue <- 4.32e-08 #设置p阈值
pdf("df_volcano.pdf")
plot(df$fd,-log10(df$p_value_adjust),col="#00000033",
     pch=19,xlab =paste("log2 (fold change)"),ylab = "-log10 (p_value_adjust)" )
up <- subset(df,df$p_value_adjust< pvalue & df$fd>cut.fd)
up
down <- subset(df,df$p_value_adjust<pvalue & df$fd<as.numeric(cut.fd*(-1)))
#View(up)
#View(down)

#绘制
points(up$fd,-log10(up$p_value_adjust),col=1,
       bg = brewer.pal(9,"YlOrRd")[6],pch=21,cex=1.5)
points(down$fd,-log10(down$p_value_adjust),col=1,
       bg = brewer.pal(11,"RdBu")[9],pch=21,cex=1.5)
abline(h = -log10(pvalue),v=c(-1*fd,fd),lty=2,lwd=1)
dev.off()
setwd("E:/R_workbook/")
library(RColorBrewer)
library(ggplot2)
#display.brewer.all()
LU_matrix <- matrix(runif(200,0,0.5),nrow = 10,ncol = 20)
RD_matrix <- matrix(runif(340,-0.5,0),nrow = 17,ncol = 20)
RU_matrix <- matrix(runif(20,-0.5,0),nrow = 10,ncol = 2)
LD_matrix <- matrix(runif(34,0,0.5),nrow = 17,ncol = 2)
data <- rbind(cbind(LU_matrix,RU_matrix),cbind(RD_matrix,LD_matrix))
names <- read.table("rownames.txt")
library(ComplexHeatmap)
library(circlize)
col_fun <- colorRamp2(c(-0.5,-0.1,0.1,0.5),c("#5296cc","#cad5f9","#fdedf6","#F165B2"))
p_data <- matrix(runif(27*22,0,0.1),nrow = 27,ncol = 22)
T_data <- matrix(runif(27*22,0,0.1),nrow = 27,ncol = 22)
T_data <- T_data>0.05
rownames(data) <- names[1:27,1]
colnames(data) <- names[27:48,1]
#View(data)
pdf("heatmap.pdf",height = 6,width = 10)
Heatmap(data,
        col = col_fun,
        #设置格子的边框颜色和粗细
        rect_gp = gpar(col="white",lwd = 1),
        #调整聚类树的高度
        column_dend_height = unit(2,"cm"),
        #调整行标签和列标签的大小
        row_names_gp = gpar(fontsize = 7,fontface="italic",
                            col=c(rep("#ff339f",10),rep("#5facee",20))),
        column_names_gp = gpar(fontsize = 7),
        row_dend_width = unit(2,"cm"),
        cell_fun = function(j, i, x, y, width, height, fill){
            if(p_data[i,j]<0.01){
                grid.text(sprintf("*", p_data[i, j]), x, y, gp = gpar(fontsize = 10))
                
            }else if(p_data[i,j]<0.05){
                grid.text(sprintf("+", p_data[i, j]), x, y, gp = gpar(fontsize = 10))
                
            }else{
                grid.text(sprintf("", p_data[i, j]), x, y, gp = gpar(fontsize = 10))
            }
            
        }
        )
lgd <- Legend(col_fun = col_fun,
              #图例的值显示
              at=c(-0.5,0,0.5),
              title = "spearman's correction",
              legend_height = unit(2,"cm"),
              #标题相对图例位置
              title_position = "topcenter",
              title_gp = gpar(fontsize=8),
              labels_gp =  gpar(fontsize = 8),
              ##横向排列
              direction = "horizontal",
              grid_height = unit(4,"mm")
              )
draw(lgd,x = unit(0.9,"npc"),y = unit(0.9,"npc"))              
dev.off()
#install.packages("RColorBrewer")
setwd("E:/R_workbook/")
library(RColorBrewer)
library(ggplot2)
#display.brewer.all()
data <- read.table("dataset_volcano.txt",row.names = 1,header = T)
#data$label <- c(rownames(order(data$p_value_adjust))[1:10],rep(NA,nrow(data)-10))
#c(rownames(order(data$p_value_adjust))[1:10])),rep(NA,(nrow(data)-10))))
order_index = order(data$p_value_adjust)[1:10]
new_name = rownames(data)[order_index]
names = c(new_name,rep(NA,nrow(data)-10))
data$label <- names
data$significant = "stable"
data$significant[data$fd > 1.2 & data$p_value_adjust<0.00000005] <-  "up"
data$significant[data$fd < -1.2 & data$p_value_adjust<0.00000005] <-  "down"
#View(data)
ggplot(data,aes(fd,-log10(p_value_adjust)))+
    geom_hline(yintercept = -log10(0.00000005),linetype="dashed",color="#999999")+#添加水平线
    geom_vline(xintercept = c(-1.2,1.2),linetype="dashed",color="#999999")+#添加垂直线
    geom_point(aes(size=-log10(p_value_adjust),color=significant))+
    #scale_color_gradient2(low = "#33429a",mid = "#f9ed36",high = "#af1f22",midpoint = 10)+
    #scale_color_gradientn(values = seq(0,1,0.2),colors = c("#39489f","#39bbec","#f9ed36","#f38466","#b81f25"))+
    scale_color_manual(values = c(brewer.pal(9,"YlOrRd")[6],"#808080",brewer.pal(11,"RdBu")[9]))+
    scale_size_continuous(range = c(1,3))+#点大小渐变
    theme_bw()+ #加边框
    theme(panel.grid = element_blank(),
          legend.position = c(0.95,0.95),#设置legend位置
          legend.box.just = "right",
          legend.justification = c("right", "top"),
          legend.key = element_rect(fill = "white", colour = "black"))+ #去掉网格
    guides(col=guide_legend(title = ""),
           size = "none")+
    geom_text(aes(label=label,
                  color=significant,size=10,vjust=1.5,hjust=1))+
    xlab("Log2FC")+
    ylab("-Log10(FDR q-value)")
ggsave("vocanol_plot.pdf",height = 10,width = 10)

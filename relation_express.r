setwd("E:/R_workbook/")
library(RColorBrewer)
library(ggplot2)
#display.brewer.all()
data <- read.table("gene_relation.txt",sep = "\t",col.names = c("group","gene","gene_other","spearman","qvalue"))

data$n_p <- rep("positive",nrow(data))
data$n_p[data$spearman<0] <-  "negative"
qvalue <- rep(NA,nrow(data))
qvalue[which(data$qvalue>0.05)] <- ">0.05"
qvalue[which(data$qvalue<0.05)] <- "<0.05"
qvalue[which(data$qvalue<0.01)] <- "<0.01"
qvalue[which(data$qvalue<0.001)] <- "<0.001"
qvalue[which(data$qvalue<0.0001)] <- "<0.0001"
table(qvalue)
data$qvalue <- qvalue
View(data)
ggplot()+
    geom_point(data = data,aes(gene,group,size=abs(spearman),fill=qvalue),color="#999999",shape=21,stroke = 1)+
    geom_point(data = data[which(data$n_p=="positive"),],aes(gene,group,size=abs(spearman),color=qvalue),shape=16)+
    scale_fill_manual(values=c("#212c5f","#3366b1","#42b0e4","#7bc6ed","#dfe1e0"))+
    scale_color_manual(values=c("#f26666","#f49699","#facccc","#facccc","#d9dbd9"))+
    theme_bw()+
    theme(panel.grid.major.x = element_blank(),
          panel.grid.minor.x = element_blank(),
          axis.text.x = element_text(angle = 45,hjust = 1),
          legend.margin = margin(20,unit = "pt")
          )+
    guides(fill=guide_legend(title = expression("Negative \ncorrection FDR \nq-value")),
           color =guide_legend(title = "Posative \ncorrection \nFDR q-value"),
           size = guide_legend(title = "spearman-s p")
           )
ggsave("relation.pdf",height = 10,width = 10)